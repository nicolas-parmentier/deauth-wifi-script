# DeAuth WiFi Script

This script is designed to perform DeAuth WiFi Attack from a Monitor Mode WiFi Adapater. Please follow the instructions below to use the script effectively.

## Prerequisites

- A Linux-compatible operating system.
- Administrative privileges (sudo) to execute the script.
- The `net-tools` and `aircrack-ng` packages need to be installed. If they are not already installed, the script will attempt to install them using `sudo apt install --quiet net-tools aircrack-ng`.

## Usage

1. Open a terminal and navigate to the directory containing the script.sh file.
2. Make the script executable by running the following command:
   `chmod +x script.sh`
3. The script will prompt you to enter a command. Choose one of the following options:
   1. `mon` - Puts your WiFi card in Monitor mode.
   2. `man` - Restores your WiFi card to Managed mode.
   3. `scan` - Scans for nearby routers and their details.
   4. `deauth` - Performs a deauthentication attack on a router or client.
4. Follow the instructions provided by the script and enter the required information as prompted
5. Depending on the chosen command, the script will execute the corresponding actions:
   1. For mon, the script will bring down the WiFi interface, set it to Monitor mode, and bring it back up.
   2. For man, the script will bring down the WiFi interface, set it to Managed mode, restart the network-manager service, and bring it back up.
   3. For scan, the script will prompt you to choose between scanning for a specific router or scanning for all routers. It will then display the results using airodump-ng.
   4. For deauth, the script will prompt you to choose between deauthenticating a specific client or deauthenticating all clients. It will then perform a deauthentication attack using aireplay-ng.
6. Please exercise caution and use this script responsibly and legally. Be aware of and adhere to the laws and regulations in your country regarding wireless network security.

Note: The script contains a predefined WiFi interface name (WIFI). Modify the value of this variable (WIFI="wlx00c0ca988041") with the name of your WiFi card before running the script. You can find the name of your WiFi card by running the command ip a in the terminal.
